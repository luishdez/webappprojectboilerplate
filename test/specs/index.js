/**
 * Unit Tests for index component
 *
 */
describe('Component: index', function () {

  describe('Markup', function () {

    it('should exists an element with .testContent class', function () {
      $('.testContent').length.should.be.ok;
    });

    it('should has text Hello World!', function () {
      $('.testContent').text().should.equal('Hello World!');
    });
  });

  describe('Setting configs', function () {

    before(function (done) {
      $('.testContent').addClass('elegant');
      done();
    });

    it('should exists an element with .testContent', function () {
      $('.testContent').hasClass('elegant').should.be.ok;
    });
  });

  describe('Setting configs', function () {

    before(function (done) {
      $('.testContent').animate({
        'width': '200'
      }, done);
    });

    it('should has a width of 200px after animate', function () {
      $('.testContent').width().should.be.equal(200);
    });
  });

  describe('Bind and Callback controlls', function () {

    var Clicked;

    before(function (done) {
      $('.testContent').click(function () {
        Clicked = true;
        $(this).text('Yeah Clicked!');
      });
      done();
    });

    it.skip('should has bind a click event ', function () {
    });

    before(function (done) {
      $('.testContent').trigger('click');
      done();
    });

    it('should has a Yeah Clicked! text after clicked', function () {
      $('.testContent').text().should.equal('Yeah Clicked!');
    });
  });

});
