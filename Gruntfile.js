module.exports = function( grunt ) {

  'use strict';

  grunt.loadNpmTasks('grunt-recess');
  grunt.loadNpmTasks('grunt-html');

  grunt.initConfig({

    bower: {
      dir: 'app/components'
    },

    coffee: {
      compile: {
        files: {
          'temp/scripts/*.js': 'app/scripts/**/*.coffee'
        },
        options: {
          basePath: 'app/scripts'
        }
      }
    },

    htmllint: {
      all: [
        "app/*.html",
        "app/templates/*.html",
        "app/layout/*.html",
        "tests/**/*.html"
      ]
    },

    recess: {
      dist: {
        src: 'app/styles/less/main.less',
        dest: 'app/styles/css/main.css',
        options: {
          compile: true
        }
      },
      linter: {
        src: 'app/styles/css/main.css',
        options: {
          compile: false,
          compress: false,
          noIDs: false,
          noJSPrefix: true,
          noOverqualifying: false,
          noUnderscores: false,
          noUniversalSelectors: false,
          prefixWhitespace: false,
          strictPropertyOrder: false,
          stripColors: true,
          zeroUnits: false
        }
      }
    },

    manifest: {
      dest: ''
    },

    mocha: {
      all: ['test/**/*.html']
    },

    watch: {
      coffee: {
        files: 'app/scripts/**/*.coffee',
        tasks: 'coffee reload'
      },
      recess: {
        files: [
          'app/less/**/*.less',
          'app/styles/less/*.less',
          'app/styles/less/**/*.less'
        ],
        tasks: 'recess reload'
      },
      reload: {
        files: [
          'app/*.html',
          'app/templates/*',
          'app/sections/*',
          'app/styles/**/*.css',
          'app/scripts/**/*.js',
          'app/images/**/*',
          'test/**/*.js',
          'test/**/*.css',
          'test/**/*.html'
        ],
        tasks: 'reload'
      }
    },

    lint: {
      files: [
        'Gruntfile.js',
        'app/scripts/**/*.js',
        'spec/**/*.js'
      ]
    },

    jshint: {
      options: {
        "validthis": true,
        "laxcomma" : true,
        "laxbreak" : true,
        "browser"  : true,
        "eqnull"   : true,
        "debug"    : true,
        "devel"    : true,
        "boss"     : true,
        "expr"     : true,
        "indent"   : 2,
        "curly"    : true,
        "white"    : true,
        "asi"      : true
      },
      globals: {
        jQuery: true
      }
    },

    staging: 'temp',
    output: 'dist',

    mkdirs: {
      staging: 'app/'
    },

    // Below all paths are relative to the stagin directory.
    css: {
      'styles/main.css': ['styles/**/*.css']
    },

    rev: {
      js: 'scripts/**/*.js',
      css: 'styles/**/*.css',
      img: 'images/**'
    },

    'usemin-handler': {
      html: 'index.html'
    },

    usemin: {
      html: ['*.html', 'layouts/**/*.html', 'templates/**/*.html'],
      css: ['*.css', 'styles/**/*.css', 'layouts/**/*.css', 'templates/**/*.css']
    },

    html: {
      files: ['**/*.html']
    },

    img: {
      dist: '<config:rev.img>'
    },

    rjs: {
      optimize: 'none',
      baseUrl: './scripts',
      wrap: true,
      name: 'main'
    },

    concat: {
      dist: ''
    },

    min: {
      dist: ''
    }
  });

  grunt.registerTask('pre-commit', 'lint htmllint test');

  grunt.registerTask('test', 'server:phantom mocha');

  grunt.registerTask('compass', 'recess');
};
