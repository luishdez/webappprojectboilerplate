!function ($) {

  'use strict';

  var Test = function (element) {
      this.element = $(element);
  };

  Test.prototype = {

    constructor: Test,
    position: false,

    init: function () {
      this.bindButton();
      this.makeChilderFocusable();
    },

    bindButton: function () {
      this.element.find('button').click( function(){
        this.coolAction();
      }.bind(this));
    },

    coolAction: function () {
      this.element.text('Yeah!');
    },

    makeChilderFocusable: function () {
      this.element.find('div').attr('tabindex', 0);
    },
  };

  $.fn.test = function (option, setter) {
    return this.each(function () {
      var $this = $(this);
      var data = $this.data('test');

      if (!data) {
        $this.data('test', (data = new Test(this)));
      }

      if (typeof option === 'string') {
        data[option](setter);
      }
    });
  };

  $.fn.test.Constructor = Test;

  $(function () {
    $('[data-plugin="test"]').test('init');
  });

}(window.jQuery);
